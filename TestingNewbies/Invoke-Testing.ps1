[CmdletBinding(DefaultParameterSetName = "Generate")]

param(
    $TestFile = ".\Functions.tests.ps1",
    $FunctionsFile = ".\Functions.ps1",

    # Used to generate a new blank functions file
    [Parameter(ParameterSetName = "Generate")]
    [switch]$GenerateFunctionsTemplate,

    # Used to test a functions file
    [Parameter(ParameterSetName = "Run")]
    [switch]$Run

)

if ($PSCmdlet.ParameterSetName -eq "Generate") {
    $Template = @"
function {0} {{
    <#
    .SYNOPSIS
{1}
    #>
    param()



}}

"@

    $Script = @{
        Path       = $TestFile
        Parameters = @{
            ListTestsOnly = $true
        }
    }

    $Tests = Invoke-Pester -Script $Script -PassThru -Show None |
        Select-Object -expand Testresult

    $FileContent = foreach ($Group in $Tests | Group-Object -Property Context) {

        $FunctionComment = ""
        foreach ($Test in $Group.Group) {
            $FunctionComment += "    {0}`n" -f $Test.Name
        }

        $Template -f $Group.Name, $FunctionComment
    }

    Set-Content -Path $FunctionsFile -Value $FileContent
}

if ($PSCmdlet.ParameterSetName -eq "Run") {
    $Script = @{
        Path       = $TestFile
        Parameters = @{
            FunctionsFile = $FunctionsFile
        }
    }
    Invoke-Pester -Script $Script
}