#Requires -Modules Assert
#Requires -Modules @{ ModuleName="Pester"; ModuleVersion="4.0.0" }

param(
    [switch]$ListTestsOnly,
    $FunctionsFile = ".\Functions.ps1"
)

if (-not $ListTestsOnly) {
    . $FunctionsFile
}


Describe "Syntax tests" {

    Context "Get-FolderContents" {
        it "Lists executables in C:\ without looking in any directories" {
            $Results = Get-FolderContents | Select-Object FullName
            $Correct = Get-ChildItem -Path C:\*.exe | Select-Object FullName
            Assert-Equivalent -Expected $Correct -Actual $Results
        }
        it "Only has fullname and length properties" {
            $Results = Get-FolderContents
            $Correct = Get-ChildItem -Path C:\*.exe | Select-Object Name, Length
            Assert-Equivalent -Expected $Correct -Actual $Results
        }
    }

    Context "New-PSCustomObject" {
        it "Creates PSCustomObject with Property 'Name' set to 'posh' and 'Rank' set to 'guru'" {
            $Results = New-PSCustomObject
            $Correct = [PSCustomObject]@{
                Name = "posh"
                Rank = "guru"
            }
            Assert-Equivalent -Expected $Correct -Actual $Results
        }
    }


}
