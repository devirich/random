<#
.SYNOPSIS
A tool to output powershell code with syntax highlighting and optionally mark lines

.DESCRIPTION
Generate-CodeCoverageDetail will take a powershell script file and colorize its syntax and optionally mark lines
(Such as missed code coverage lines from Pester testing via -CodeCoverage).

Inspired by Coverage.py ( https://coverage.readthedocs.io/ and
https://nedbatchelder.com/files/sample_coverage_html/index.html )

Original Author: Lee Holmes, http://www.leeholmes.com/blog/MorePowerShellSyntaxHighlighting.aspx

Modified by: Helge Klein, http://blogs.sepago.de/helge/

Further modified by Devin Rich, https://gitlab.com/devirich/random/blob/master/Generate-CodeCoverageDetail.ps1


.PARAMETER Path
The object to operate on.

.PARAMETER InputObject
The object to operate on.

.PARAMETER MarkLine
Description of each of the parameters

.INPUTS
[string[]]

.OUTPUTS
[string[]]

.EXAMPLE
PS> .\Generate-CodeCoverageDetail.ps1 -Path .\Test.ps1

Saves file at .\Test.ps1.html

.EXAMPLE
PS> .\Generate-CodeCoverageDetail.ps1 -Path .\Test.ps1 -MarkLine 4,5

Saves file at .\Test.ps1.html and marks lines 4 and 5

.EXAMPLE
PS> .\Generate-CodeCoverageDetail.ps1 | clip

Prompts for the lines to colorize and copies to clipboard

.EXAMPLE
PS> "ls","Write-host 'hi'" | .\Generate-CodeCoverageDetail -MarkLine 1

Uses pipeline to generate output

        <head>
        <link rel="stylesheet" href="http://static.rich.soon.it/CodeCoverageDetailStyle.css" type="text/css">
        </head>
        <div id="source">
        <table>
        <tr>
        <td class="linenos">
<p id="line1" class=""><a href="#line1">1</a></p>

<p id="line2" class=""><a href="#line2">2</a></p>

</td><td class="text">
<p id="t1" class="pln mis"><span style='color:#0000FF'>ls</span><br /></p>

<p id="t2" class="pln "><span style='color:#0000FF'>Write-host</span><span style='color:#000000'>&nbsp;</span><span style='color:#8B0000'>&#39;hi&#39;</span></p>

        </td>
        </tr>
        </table>
        </div>

.LINK
https://gitlab.com/devirich/random

#>

[CmdletBinding(
    SupportsShouldProcess,
    DefaultParameterSetName = 'InputObject'
)]
param(
    [Parameter(
        ParameterSetName = 'InputObject',
        Position = 0,
        Mandatory,
        ValueFromPipeline
    )]
    [string[]]$InputObject,

    [Parameter(
        ParameterSetName = 'Path',
        Position = 0,
        Mandatory
    )]
    [string]$Path,


    [Parameter(
        Position = 1
    )]
    [Alias("MissingCommandLineNumber")]
    [int[]]$MarkLine


)

begin {
    # Load required assemblies
    [void] [System.Reflection.Assembly]::Load(
        "System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    )

    $tokenColours = @{
        'Attribute'          = '#FFADD8E6'
        'Command'            = '#FF0000FF'
        'CommandArgument'    = '#FF8A2BE2'
        'CommandParameter'   = '#FF000080'
        'Comment'            = '#FF006400'
        'GroupEnd'           = '#FF000000'
        'GroupStart'         = '#FF000000'
        'Keyword'            = '#FF00008B'
        'LineContinuation'   = '#FF000000'
        'LoopLabel'          = '#FF00008B'
        'Member'             = '#FF000000'
        'NewLine'            = '#FF000000'
        'Number'             = '#FF800080'
        'Operator'           = '#FFA9A9A9'
        'Position'           = '#FF000000'
        'StatementSeparator' = '#FF000000'
        'String'             = '#FF8B0000'
        'Type'               = '#FF008080'
        'Unknown'            = '#FF000000'
        'Variable'           = '#FFFF4500'
    }

    # Generate an HTML span and append it to HTML string builder
    function New-HtmlSpan {
        param(
            $block,
            $tokenColor
        )
        if (($tokenColor -eq 'NewLine') -or ($tokenColor -eq 'LineContinuation')) {
            if ($tokenColor -eq 'LineContinuation') {
                $null = $codeBuilder.Append('`')
            }

            $null = $codeBuilder.Append("<br />`n")
        }
        else {
            $block = [System.Web.HttpUtility]::HtmlEncode($block)
            $block = $block.Replace(' ', '&nbsp;').Replace("`t", '&nbsp;' * 4)

            $htmlColor = $tokenColours[$tokenColor].ToString().Replace('#FF', '#')

            if ($tokenColor -eq 'String') {
                $lines = $block -split "\r?\n"
                $block = ""

                $multipleLines = $false
                foreach ($line in $lines) {
                    if ($multipleLines) {
                        $block += "<br />`n"
                    }


                    $block += $line
                    $multipleLines = $true
                }
            }

            $null = $codeBuilder.Append("<span style='color:$htmlColor'>$block</span>")
        }
    }

    function Main {
        param(
            $CodeBlock,
            $MarkLine
        )
        $text = $CodeBlock


        trap { break }

        # Do syntax parsing.
        $errors = $null
        $tokens = [system.management.automation.psparser]::Tokenize($Text, [ref] $errors)

        # Initialize HTML builder.
        $codeBuilder = new-object system.text.stringbuilder

        # Iterate over the tokens and set the colors appropriately.
        $position = 0
        foreach ($token in $tokens) {
            if ($position -lt $token.Start) {
                $block = $text.Substring($position, ($token.Start - $position))
                $tokenColor = 'Unknown'
                New-HtmlSpan $block $tokenColor
            }

            $block = $text.Substring($token.Start, $token.Length)
            $tokenColor = $token.Type.ToString()

            New-HtmlSpan $block $tokenColor

            $position = $token.Start + $token.Length
        }

        # Build the entire syntax-highlighted script
        $code = $codeBuilder.ToString() -split "`r?`n"
        $Wrapper = @"
        <head>
        <link rel="stylesheet" href="http://static.rich.soon.it/CodeCoverageDetailStyle.css" type="text/css">
        </head>
        <div id="source">
        <table>
        <tr>
        <td class="linenos">
"@
        $mid = '</td><td class="text">'
        $tail = @"
        </td>
        </tr>
        </table>
        </div>
"@

        $LineNumberCell = foreach ($Line in $Code) {
            $i++
            '<p id="line{0}" class=""><a href="#line{0}">{0}</a></p>{1}' -f $i, "`n"
        }

        $TextCell = foreach ($Line in $Code) {
            $j++
            '<p id="t{0}" class="pln {2}">{1}</p>
            ' -f $j,
            (('&nbsp;', $Line)[[Boolean]$Line]),
            (('', 'mis')[$j -in $MarkLine])
        }
        $Code = $Wrapper, $LineNumberCell, $mid, $TextCell, $tail
        # Replace tabs with three blanks
        #$code    = $code -replace "\t","&nbsp;&nbsp;&nbsp;&nbsp;"
        #$Code[13] = "<span style='background-color: #ff0;'>{0}</span>" -f $Code[13]
        # Write the HTML to a file
        #$code | set-content -Path "$Path.html"
        $code
    }

    if ($PSCmdlet.ParameterSetName -eq "InputObject") {
        $CodeBlock = [System.Collections.Generic.List[System.Object]]::new()
    }
}

process {
    if ($PSCmdlet.ParameterSetName -eq "InputObject") {
        foreach ($Item in $InputObject) {
            $CodeBlock.Add([string]$Item)
        }
    }
}

end {
    if ($PSCmdlet.ParameterSetName -eq "Path") {
        $CodeBlock = Get-Content -Path $Path
    }

    $CodeBlock = $CodeBlock -join "`r`n"
    $PrettyHtml = Main -CodeBlock $CodeBlock -MarkLine $MarkLine

    if ($PSCmdlet.ParameterSetName -eq "InputObject") {
        $PrettyHtml
    }
    elseif ($PSCmdlet.ParameterSetName -eq "Path") {
        $PrettyHtml | Set-Content -Path "$Path.html"
    }
}

# Why does CBH not work?


